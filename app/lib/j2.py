import glob
import json
import re
from inspect import getmembers, isfunction
from pydoc import importfile

import jinja2

from app.config import log

def render_dict(d, data):
    r = {}
    for k, v in d.items():
        if isinstance(v, dict):
            r.update({k: render_dict(v, data)})
        elif isinstance(v, list):
            r.update({k: render_list(v, data)})
        elif isinstance(v, str):
            r.update({k: render_str(v, data)})
        else:
            r.update({k: v})
    return r

def render_list(l, data):
    r = []
    for v in l:
        if isinstance(v, dict):
            r.append(render_dict(v, data))
        elif isinstance(v, list):
            r.append(render_list(v, data))
        elif isinstance(v, str):
            r.append(render_str(v, data))
        else:
            r.append(v)
    return r

def render_str(s, data, allow_nonstr=True):
    nonstr = re.match(r'^\{\{.+\}\}$', s) and allow_nonstr
    tpl = jinja2.Environment(
        loader = jinja2.BaseLoader,
        undefined = jinja2.StrictUndefined,
        trim_blocks = not nonstr,
        finalize = json.dumps if nonstr else None,
    )
    tpl.globals.update(__get_functions('functions'))
    tpl.filters.update(__get_functions('filters'))
    tpl.tests.update(__get_functions('tests'))
    result = tpl.from_string(s).render(data)
    return json.loads(result) if nonstr else result

def render_file(path, file, data):
    tpl = jinja2.Environment(
        loader = jinja2.FileSystemLoader(path),
        undefined = jinja2.StrictUndefined,
        trim_blocks = True
    )
    tpl.globals.update(__get_functions('functions'))
    tpl.filters.update(__get_functions('filters'))
    tpl.tests.update(__get_functions('tests'))
    return tpl.get_template(file).render(data)

def __get_functions(path: str):
    result = {}
    for file in glob.glob(f'/repos/config/{path}/*.py'):
        module = importfile(file)
        for function in getmembers(module, isfunction):
            log('debug', f'Loading j2 {path} function from {file}:{function[0]}')
            result.update({function[0]: getattr(module, function[0])})
    return result
