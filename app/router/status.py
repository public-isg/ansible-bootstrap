import os

from fastapi import APIRouter, status

from app.config import env
from app.lib import config
from app.lib import git

router = APIRouter()

@router.get(
    '/health',
    summary = 'Test if the application is running',
    status_code = status.HTTP_204_NO_CONTENT,
    responses = {
        500: {'content': {'text/plain': {'schema': {'type': 'string'}}}},
    },
)
def get_health() -> None:
    '''
    Will check if the API is working, if the repositories are pulled and
    readable and if the main config file can be parsed.
    '''
    assert os.path.isdir('/repos/config/.git')
    assert os.path.isdir('/repos/inventory/.git')
    config.read()


@router.post(
    '/pull/config',
    summary = 'Pull the config repository',
    status_code = status.HTTP_204_NO_CONTENT,
    responses = {
        500: {'content': {'text/plain': {'schema': {'type': 'string'}}}},
    },
)
def pull_config() -> None:
    git.update('/repos/config', env('config_git_url'), env('config_git_branch'))


@router.post(
    '/pull/inventory',
    summary = 'Pull the inventory repository',
    status_code = status.HTTP_204_NO_CONTENT,
    responses = {
        500: {'content': {'text/plain': {'schema': {'type': 'string'}}}},
    },
)
def pull_inventory() -> None:
    git.update('/repos/inventory', env('inventory_git_url'), env('inventory_git_branch'))
