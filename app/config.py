import logging

from pydantic_settings import BaseSettings, SettingsConfigDict

# pylint: disable=global-statement,global-variable-not-assigned
__ENV = None
__LOG = None

__LOGLEVELS = {
    'critical': logging.CRITICAL,
    'error': logging.ERROR,
    'warning': logging.WARNING,
    'info': logging.INFO,
    'debug': logging.DEBUG,
}

class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_prefix='bootstrap_')

    root_path: str = '/'

    archive_secret: str = ''

    git_ssh_key_file: str = '/root/.ssh/id_rsa'
    git_ssh_known_hosts_file: str = '/root/.ssh/known_hosts'

    config_git_url: str = 'https://user:pass@git.example.com/bootstrap-config.git'
    config_git_branch: str = 'main'
    config_git_autopull: bool = False # pull before every GET of a template file

    inventory_git_url: str = 'https://user:pass@git.example.com/inventory.git'
    inventory_git_branch: str = 'main'
    inventory_git_autopull: bool = False # pull before every GET of a template file

def env(var: str, default = None):
    global __ENV
    if __ENV is None:
        __ENV = Settings()
    return getattr(__ENV, var, default)

def log(level: str, message: str):
    global __LOG
    if __LOG is None:
        __LOG = logging.getLogger('uvicorn.error')
    __LOG.log(__LOGLEVELS.get(level, logging.INFO), message)
